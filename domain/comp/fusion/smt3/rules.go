package smt3

import demon "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/demon"

type rules struct {
}

func (r rules) Fuse(demon1 demon.Demon, demon2 demon.Demon) (demon.Demon, error) {
	return demon.Demon{}, nil
}

func NewRules() rules {
	return rules{}
}
