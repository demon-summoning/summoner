package fusion

import (
	"fmt"

	smt "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	demon "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/demon"
)

type Rules struct {
	fusionTable map[*demon.Race]map[*demon.Race]*demon.Race
	game        smt.Game
	moonPhase   smt.MoonPhase
	item        smt.Item
}

func (r Rules) lv(lv1 int, lv2 int) int {
	return (lv1 + lv2) / 2
}

func (r Rules) Race(demon1 *demon.Demon, demon2 *demon.Demon) (*demon.Race, error) {
	race, exist := r.fusionTable[demon1.Race][demon2.Race]
	if exist {
		return race, nil
	} else {
		return nil, fmt.Errorf("race %s or/and %s dont't exist in the game %s",
			demon1.Race.Race, demon2.Race.Race, r.game.String())
	}
}
