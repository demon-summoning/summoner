package fusion

import demon "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/demon"

type SpecialRules interface {
	Fuse(demon1 demon.Demon, demon2 demon.Demon) (demon.Demon, error)
}
