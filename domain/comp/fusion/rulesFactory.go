package fusion

import (
	"fmt"

	smt "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	smt3 "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/comp/fusion/smt3"
)

func NewRules(game smt.Game) (Rules, SpecialRules, error) {
	switch game {
	case smt.Smt3:
		return Rules{fusionTable: smt3.FusionMap(),
				game:      smt.Smt3,
				moonPhase: smt.MoonPhase{},
				item:      smt.Item{}},
			smt3.NewRules(),
			nil
	default:
		return Rules{}, smt3.NewRules(), fmt.Errorf("game %s is not supported", game)
	}
}
