package smt3

import (
	"testing"

	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/comp/fusion"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/comp/fusion/smt3"
	"gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/demon"
	demonsmt3 "gitlab.com/constraintAutomaton/demon-summoning/demon-summoning/domain/demon/smt3"
)

const A_NAME string = ""

var A_STAT = demon.Stats{}

var AN_AFFINITY = demon.Affinity{}

const AN_HP_POOL = 0
const AN_MP_POOL = 0
const A_LV = 0
const A_COST = 0

var A_GAME = domain.Smt3

const A_DESCRIPTION = ""

var A_VALID_RACE = demonsmt3.Aeros()
var AN_INVALID_RACE = demon.Race{Game: domain.Smt3, Race: "Foo"}

func TestRaceFusionNaive(t *testing.T) {
	fusionTable := smt3.FusionMap()
	for race1, fusionRow := range fusionTable {
		for race2, expectedRace := range fusionRow {
			rules := provideRules()
			demon1 := provideADemon(race1)
			demon2 := provideADemon(race2)
			result, _ := rules.Race(demon1, demon2)
			if result != expectedRace {
				t.Errorf("race %s and race %s is suppose to give %s and not %s",
					race1, race2, expectedRace, result)
			}

		}
	}
}

func TestUnvalidRace(t *testing.T) {
	demonWithValidRace := provideADemon(A_VALID_RACE)
	demonWithInvalidValidRace := provideADemon(AN_INVALID_RACE)
	rules := provideRules()
	_, err := rules.Race(demonWithValidRace, demonWithInvalidValidRace)
	if err == nil {
		t.Error("Invalid smt3 race should return an error")
	}
	_, err = rules.Race(demonWithInvalidValidRace, demonWithInvalidValidRace)
	if err == nil {
		t.Error("Invalid smt3 race should return an error")
	}
}

func provideADemon(race demon.Race) demon.Demon {
	return demon.Demon{A_NAME,
		A_STAT,
		AN_AFFINITY,
		race,
		AN_HP_POOL,
		AN_MP_POOL,
		A_LV,
		A_COST,
		A_GAME,
		A_DESCRIPTION}
}
func provideRules() fusion.Rules {
	rules, _, _ := fusion.NewRules(domain.Smt3)
	return rules
}
